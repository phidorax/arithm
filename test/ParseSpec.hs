module ParseSpec (main, spec) where

import Test.Hspec

import Parse
import Expr

main :: IO ()
main = hspec spec

spec :: Spec
spec = 
    describe "parse" $ do
        it "parse + 2 2" $ parse "+ 2 2" `shouldBe` (((Add (Val 2) (Val 2)), "")::(Expr, String))
