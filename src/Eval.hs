-- |
-- Module: Eval
-- Eval module 
module Eval where

import Expr

-- | Evaluate the operation
eval :: Expr -> Int
eval (Val x) = x
eval (Add e1 e2) = eval e1 + eval e2
eval (Mul e1 e2) = eval e1 * eval e2

