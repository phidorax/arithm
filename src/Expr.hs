-- |
-- Module: Expr
-- This is Expr module
module Expr where

-- | This is Expr data
data Expr
    = Val Int
    | Add Expr Expr
    | Mul Expr Expr
    deriving (Eq, Show)

